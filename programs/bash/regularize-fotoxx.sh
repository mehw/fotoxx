#!  /bin/bash -x
# To (a) rename fotoxx-*.cc as fotoxx.c and (b) patch references

if [ $(ls programs/fotoxx/fotoxx-*.cc | wc -l) -ne 1 ]
then
  echo "ERROR this fixes exactly one fotoxx-*.c"
  ls programs/fotoxx/fotoxx*.cc
  exit 2
fi

path=programs/fotoxx
fnA=$(cd $path; ls fotoxx-*.cc)
fnB=${fnA%-*.cc}.cc

mv $path/$fnA $path/$fnB


fnA=${fnA/[.]/[.]} ; # escape the dot in PCRE

perl -pli.bak -E "s($fnA)($fnB);"  $path/Makefile

# comments in .PO Not always carefully updated  so be permissive
echo patching Locales ...
fnA='fotoxx[-0-9.]+cc'
perl -pli.bak -E "s($fnA)($fnB);"  programs/fotoxx/locales/translate-*.po

ack -l '\015' | xargs dos2unix

